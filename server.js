const express = require('express');
const path = require('path');
const app = express();
const dotenv = require('dotenv');
const mongoose = require('mongoose');

// const compiler = webpack(config);

// Import Routes
const logRoute = require('./routes/userAuth/log');
         
// Middleware
app.use(express.json());

// app.use(require('webpack-dev-middleware')(compiler, {
//     noInfo: true,
//     publicPath: config.output.publicPath
// }));

// app.use(require('webpack-hot-middleware')(compiler, {
//     log: console.log,
//     path: '/__webpack_hmr',
//     heartbeat: 10 * 1000
// }));

// Route Middlewares
app.use('/api/log', logRoute);

dotenv.config();

// Connect DB
mongoose.connect(
    process.env.DB_CONNECT, 
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => console.log('connected to db!')
);

const port = process.env.PORT || 3000;

app.get('/', (req, res) => {
    res.send('Hello World!')
})

// const DIST_DIR = path.join(__dirname, './'); // NEW
// const HTML_FILE = path.join(DIST_DIR, 'index.html'); // NEW

// app.use(express.static(DIST_DIR)); // NEW

// app.get('/*', (req, res) => {
//     res.sendFile(HTML_FILE); // EDIT
// });

app.listen(port, function () {
    console.log('App listening on port: ' + port);
});
