const mongoose = require('mongoose');

const LogSchema = new mongoose.Schema({
    notifType: {
        type: String,
    },
    temperature: {
        type: String
    },
    humidity:{
        type: String
    },
    lightStatus: {
        type: String
    },
    fanDetection: {
        type: String
    },
    machineStatus: {
        type: String
    },
    machineIp: {
        type: String
    },
    description: {
        type: String
    }, 
    date:{
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Log', LogSchema, "Logs");