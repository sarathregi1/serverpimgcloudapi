const router = require('express').Router();
const Log = require('../../models/user/logModel');
const nodemailer = require("nodemailer");

async function verificationEmail(content) {

    let transporter = nodemailer.createTransport({
        host: "mail.name.com",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: process.env.EMAILU,
            pass: process.env.EMAILP,
        },
    });

    let info = await transporter.sendMail({
        from: '"Server room management system" <info@untitledmedical.com>', // sender address
        to: "sarathregi1@gmail.com", // list of receivers
        subject: "serverPiMG", // Subject line
        html: content, // html body
    });

    console.log("Message sent: %s", info.messageId);

    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
}


router.post('/roomDataInput', async (req, res) => {
    
    const log = new Log({
        notifType: req.body.notifType,
        temperature: req.body.temperature,
        humidity: req.body.humidity,
        lightStatus: req.body.lightStatus,
        fanDetection: req.body.fanDetection,
        machineStatus: req.body.machineStatus,
        machineIp: req.body.machineIp,
        description: req.body.description
    });

    try{
        await log.save();
        res.send("success")
    }catch(err){
        res.status(400).send(err);
    }

    if (req.body.notifType == "unAuthorized") {

        const content = "Server was trying to be accessed by unauthorized user";
        await verificationEmail(content).catch(console.error);

    }
    else if (req.body.notifType == "authorized") {

        const content = "Server was accessed by authorized user";
        
        await verificationEmail(content).catch(console.error);
    }
    else if (req.body.notifType == "systemOff") {
        const content = "A server has gone offline";
        
        await verificationEmail(content).catch(console.error);
    }
    else if (req.body.notifType == "systemOn") {
        const content = "A server has come online";
        
        await verificationEmail(content).catch(console.error);
    }


});

module.exports = router;

